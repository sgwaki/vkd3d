[require]
shader model >= 4.0

[pixel shader]
uint4 v;

float4 main() : sv_target
{
    switch (v.x)
    {
    case 0:
        return 3.0;
    case 1:
        return 4.0;
    default:
        return 5.0;
    }
}

[test]
uniform 0 uint4 3 0 0 0
draw quad
probe all rgba (5.0, 5.0, 5.0, 5.0)
uniform 0 uint4 1 0 0 0
draw quad
probe all rgba (4.0, 4.0, 4.0, 4.0)
uniform 0 uint4 0 0 0 0
draw quad
probe all rgba (3.0, 3.0, 3.0, 3.0)

% falling through is only supported for empty case statements
[pixel shader]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};

    switch (v.x)
    {
    case 0:
    case 1:
        c.x += 0.1f;
        break;
    }

    return c;
}

[test]
uniform 0 uint4 2 0 0 0
draw quad
probe all rgba (1.0, 2.0, 3.0, 4.0)
uniform 0 uint4 1 0 0 0
draw quad
probe all rgba (1.1, 2.0, 3.0, 4.0)
uniform 0 uint4 0 0 0 0
draw quad
probe all rgba (1.1, 2.0, 3.0, 4.0)

% case value evaluation
[pixel shader]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};

    switch (v.x)
    {
    case 1+1:
        c += 0.1f;
        break;
    case 0:
        c += 0.2f;
        break;
    }

    return c;
}

[test]
uniform 0 uint4 2 0 0 0
todo(sm>=6) draw quad
probe all rgba (1.1, 2.1, 3.1, 4.1)
uniform 0 uint4 1 0 0 0
todo(sm>=6) draw quad
probe all rgba (1.0, 2.0, 3.0, 4.0)

% floats are accepted
[pixel shader fail(sm>=6)]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};

    switch (v.x)
    {
    case 2.1f:
        c += 0.1f;
        break;
    case 0.9f:
        c += 0.2f;
        break;
    }

    return c;
}

[test]
uniform 0 uint4 2 0 0 0
todo(sm>=6) draw quad
probe all rgba (1.1, 2.1, 3.1, 4.1)
uniform 0 uint4 1 0 0 0
todo(sm>=6) draw quad
probe all rgba (1.0, 2.0, 3.0, 4.0)

[pixel shader fail(sm>=6)]
float4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};

    switch (v.x)
    {
    case 2.1f:
        c += 0.1f;
        break;
    case 0.9f:
        c += 0.2f;
        break;
    }

    return c;
}

[test]
uniform 0 float4 2.0 0.0 0.0 0.0
todo(sm>=6) draw quad
probe all rgba (1.1, 2.1, 3.1, 4.1)
uniform 0 float4 1.0 0.0 0.0 0.0
todo(sm>=6) draw quad
probe all rgba (1.0, 2.0, 3.0, 4.0)

[pixel shader fail]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};
    uint a = 1;

    switch (v.x)
    {
    case 1+a:
        c += 0.1f;
        break;
    case 0:
        c += 0.2f;
        break;
    }

    return c;
}

% duplicate cases
[pixel shader fail]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};

    switch (v.x)
    {
    case 2:
        c += 0.1f;
        break;
    case 1+1:
        c += 0.2f;
        break;
    }

    return c;
}

% multiple default cases
[pixel shader fail]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};

    switch (v.x)
    {
    default:
    case 2:
        c += 0.1f;
        break;
    case 1:
        c += 0.2f;
        break;
    default:
        break;
    }

    return c;
}

% unterminated cases
[pixel shader fail(sm<6)]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};

    switch (v.x)
    {
    case 0:
        c += 0.1f;
    case 1:
        c += 0.2f;
        break;
    }

    return c;
}

[pixel shader fail]
uint4 v;

float4 main() : sv_target
{
    switch (v.x)
    {
    case 0:
        return 3.0;
    case 1:
        return 4.0;
    case 2:
    }
    return 0.0;
}

[pixel shader fail]
uint4 v;

float4 main() : sv_target
{
    switch (v.x)
    {
    case 0:
        return 3.0;
    case 1:
        return 4.0;
    default:
    }
    return 0.0;
}

[pixel shader fail(sm<6)]
uint4 v;

float4 main() : sv_target
{
    switch (v.x)
    {
    case 0:
        return 3.0;
    case 1:
        return 4.0;
    default:
        discard;
    }
    return 0.0;
}

[pixel shader fail(sm<6)]
uint4 v;

float4 main() : sv_target
{
    switch (v.x)
    {
    case 0:
        return 3.0;
    case 1:
        return 4.0;
    case 2:
        discard;
    }
    return 0.0;
}

[pixel shader fail(sm<6)]
uint4 v;

float4 main() : sv_target
{
    switch (v.x)
    {
    case 0:
        discard;
    case 1:
        return 4.0;
    }
    return 0.0;
}

% more complicated breaks
[pixel shader]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};

    switch (v.x)
    {
    case 2:
        c += 0.1f;
        if (true) break;
        c = 9.0f;
    case 1:
        if (false) break;
        c += 0.2f;
        break;
    default:
    case 0:
        break;
    }

    return c;
}

[test]
uniform 0 uint4 2 0 0 0
todo(sm>=6) draw quad
probe all rgba (1.1, 2.1, 3.1, 4.1)
uniform 0 uint4 1 0 0 0
todo(sm>=6) draw quad
probe all rgba (1.2, 2.2, 3.2, 4.2)
uniform 0 uint4 0 0 0 0
todo(sm>=6) draw quad
probe all rgba (1.0, 2.0, 3.0, 4.0)

% switch breaks within a loop
[pixel shader]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0f, 2.0f, 3.0f, 4.0f};

    for (int i = 0; i < 4; ++i)
    {
        switch (v.x)
        {
        case 2:
            c += 1.0f;
            break;
        case 1:
            c -= 1.0f;
            break;
        default:
        case 0:
            break;
        }
    }

    return c;
}

[test]
uniform 0 uint4 2 0 0 0
todo(sm>=6) draw quad
probe all rgba (5.0, 6.0, 7.0, 8.0)

% default case placement
[pixel shader]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0f, 2.0f, 3.0f, 4.0f};

    switch (v.x)
    {
    case 2:
        c += 1.0f;
        break;
    case 1:
        c -= 1.0f;
        break;
    case 3:
    default:
    case 0:
        c += 3.0f;
        break;
    }

    return c;
}

[test]
uniform 0 uint4 0 0 0 0
todo(sm>=6) draw quad
probe all rgba (4.0, 5.0, 6.0, 7.0)
uniform 0 uint4 2 0 0 0
todo(sm>=6) draw quad
probe all rgba (2.0, 3.0, 4.0, 5.0)
uniform 0 uint4 3 0 0 0
todo(sm>=6) draw quad
probe all rgba (4.0, 5.0, 6.0, 7.0)

[pixel shader]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0f, 2.0f, 3.0f, 4.0f};

    switch (v.x)
    {
    case 2:
        c += 1.0f;
        break;
    case 1:
        c -= 1.0f;
        break;
    case 3:
    default:
        break;
    case 0:
        c += 3.0f;
        break;
    }

    return c;
}

[test]
uniform 0 uint4 3 0 0 0
todo(sm>=6) draw quad
probe all rgba (1.0, 2.0, 3.0, 4.0)
uniform 0 uint4 0 0 0 0
todo(sm>=6) draw quad
probe all rgba (4.0, 5.0, 6.0, 7.0)
uniform 0 uint4 5 0 0 0
todo(sm>=6) draw quad
probe all rgba (1.0, 2.0, 3.0, 4.0)

% 'continue' is not supported in switches
[pixel shader fail(sm<6)]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};
    uint i, j;

    for (i = 0; i < v.z; i++)
    {
        switch (v.x)
        {
            case 0:
                c += 0.1f;
                continue;
                break;
            case 1:
                c += 0.2f;
                break;
        }
    }

    return c;
}

[pixel shader]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};
    uint i, j;

    for (i = 0; i < v.z; i++)
    {
        switch (v.x)
        {
            case 0:
                for (j = 0; j < v.z; j++)
                {
                    c += 1.0f;
                    if (v.w)
                        continue;
                }
                break;
            case 1:
                c += 2.0f;
                break;
        }
    }

    return c;
}

[test]
uniform 0 uint4 0 0 3 1
todo(sm>=6) draw quad
probe all rgba (10.0, 11.0, 12.0, 13.0)
uniform 0 uint4 1 0 3 1
todo(sm>=6) draw quad
probe all rgba (7.0, 8.0, 9.0, 10.0)

% return from a switch nested in a loop
[pixel shader]
uint4 v;

float4 main() : sv_target
{
    float4 c = {1.0, 2.0, 3.0, 4.0};
    uint i, j;

    for (i = 0; i < v.z; i++)
    {
        switch (v.x)
        {
            case 0:
                c += 1.0f;
                break;
            case 1:
                c += 2.0f;
                return c;
        }
        c += 100.0f;
    }

    return c;
}

[test]
uniform 0 uint4 0 0 3 1
todo(sm>=6) draw quad
probe all rgba (304.0, 305.0, 306.0, 307.0)
uniform 0 uint4 1 0 3 1
todo(sm>=6) draw quad
probe all rgba (3.0, 4.0, 5.0, 6.0)
